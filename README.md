# BPA U3

## Getting started

ensure your system meets the following requirements

requirements
- [ ] [UI Path is installed]
- [ ] [Google Chrome extension is installed in UI Path]
- [ ] [Microsoft Excel is installed]
- [ ] [Microsoft Excel extension is installed in UI Path]

Follow the instructions:

download the .zip-Archive.
- [ ] [start the "UiPath\BPA_Ue3" Project in UI Path.]
- [ ] [store the "BPAUe3" folder at a place of your choice]
- [ ] [change the path variable in UI Path called "" to the prefix of the path to the BPAUe3-Folder]
- [ ] [run the project]